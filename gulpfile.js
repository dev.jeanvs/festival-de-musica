const { series, src, dest, watch, imagemin } = require('gulp');
const sass = require('gulp-sass');
const imagemin1 = require('gulp-imagemin');
const notify = require('gulp-notify');
const webp = require('gulp-webp');
const concat = require('gulp-concat');

//Utilidades Css
const postcss = require('gulp-postcss')
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const sourcemaps = require('gulp-sourcemaps');

//Utilidades JS
const rename = require('gulp-rename');
const terser = require('gulp-terser-js');


const paths = {
    imagenes: 'src/img/**/*',
    scss: 'src/scss/**/*.scss',
    js: 'src/js/**/*.js'
}


function watchArchivos() {
    watch(paths.scss, css); // * = La carpeta actual - ** = Todos los archivos con esa extensión
    watch(paths.js, javascript);
}

function css() {
    return src(paths.scss)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe( rename({ suffix: '.min' }))
        .pipe(dest('./build/css'))
}



function imagenes() {
    return src(paths.imagenes)
        .pipe(imagemin1())
        .pipe(dest('./build/img'))
    //.pipe(notify({ message: '¡Imagen Minificada!' }))
}

function javascript() {
    return src(paths.js)
        .pipe( sourcemaps.init() )
        .pipe( concat('bundle.js'))
        .pipe( terser())
        .pipe( sourcemaps.write('.') )
        .pipe(dest('./build/js'))
}

function versionWebp() {
    return src(paths.imagenes)
        .pipe(webp())
        .pipe(dest('./build/img'))
    //.pipe(notify({ message: '¡Version webp Lista!' }))
}


exports.css = css;
exports.imagenes = imagenes;
exports.watchArchivos = watchArchivos;

exports.default = series(css, javascript, imagenes, versionWebp, watchArchivos);


